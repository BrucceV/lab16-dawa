import { Injectable } from '@angular/core';
import { CompileShallowModuleMetadata } from "@angular/compiler";

@Injectable({
  providedIn: 'root'
})

export class HeroesService {
  private heroes: Heroe[] = [
    {
      nombre: "Aquaman",
      bio:
      "Aquaman (Arthur Curry) es un superhéroe que aparece en los cómics estadounidenses publicados por DC Comics. Creado por el artista Paul Norris y el escritor Mort Weisinger, el personaje debutó en More Fun Comics # 73 (noviembre de 1941).",
      img: "assets/img/aquaman.png",
      aparicion: "1941-11-01",
      casa: "DC"
    },
    {
      nombre: "Batman",
      bio:
      "Batman (conocido inicialmente como The Bat-Man y en español como El Hombre Murciélago) es un personaje creado por los estadounidenses Bob Kane y Bill Finger,5​ y propiedad de DC Comics.",
      img: "assets/img/batman.png",
      aparicion: "1939-05-01",
      casa: "DC"
    },
    {
      nombre: "Daredevil",
      bio:
      "Daredevil (Matthew Michael 'Matt' Murdock, llamado Dan Defensor, Diablo Defensor o Diabólico en muchas de las traducciones al español) es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics.",
      img: "assets/img/daredevil.png",
      aparicion: "1964-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Hulk",
      bio:
      "Hulk (llamado La Masa o El Hombre Increíble en muchas de las traducciones al español) es un personaje de acción ficticio, un superhéroe que aparece en los cómics estadounidenses publicados por la editorial Marvel Comics.",
      img: "assets/img/hulk.png",
      aparicion: "1962-05-01",
      casa: "Marvel"
    },
    {
      nombre: "Linterna Verde",
      bio:
      "Linterna Verde (en inglés: Green Lantern) es el alias de varios superhéroes de la ficción del Universo DC, los cuales se caracterizan por un anillo de poder y la capacidad de crear manifestaciones de luz sólida con los susodichos anillos.",
      img: "assets/img/linterna-verde.png",
      aparicion: "1940-06-01",
      casa: "DC"
    },
    {
      nombre: "Spider-man",
      bio:
      "Spider-Man (llamado Hombre Araña en muchas de las traducciones al español) es un superhéroe ficticio creado por los escritores y editores Stan Lee y Steve Ditko. Apareció por primera vez en el cómic de antología Amazing Fantasy # 15 (10 de agosto de 1962), en la Edad de Plata de los cómics.",
      img: "assets/img/spiderman.png",
      aparicion: "1962-08-01",
      casa: "Marvel"
    },
    {
      nombre: "Wolverine",
      bio:
      "Wolverine, cuyo nombre de nacimiento es James Howlett (también conocido como James Logan o simplemente Logan)4​ es un superhéroe ficticio que aparece en los cómics estadounidenses publicado por Marvel Comics, principalmente en asociación con los X-Men.",
      img: "assets/img/wolverine.png",
      aparicion: "1974-11-01",
      casa: "Marvel"
    }
  ]
  constructor() {
    console.log("Servicio listo");
   }

   getHeroes() : Heroe[] {
      return this.heroes
   }

   getHeroe(idx: string) {
      return this.heroes[idx]
   }
}

export interface Heroe {
  nombre: string,
  bio: string,
  img: string,
  aparicion: string,
  casa: string
}